import React, {Component} from 'react';

import {
  TextInput,
  View,
  StyleSheet,
  TouchableOpacity,
  KeyboardAvoidingView,
  Image,
  Text,
  Alert,
} from 'react-native';

import firebase from 'react-native-firebase';
import {StackNavigator} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import Routes from './Routes';
import type {Notification, NotificationOpen} from 'react-native-firebase';

import {requestReadSmsPermission, requestReceiveSmsPermission} from './App';

export default class Home extends Component {

  async componentDidMount() {

       const convertNumbers2English= (string)=> {
    return string.replace(/[\u0660-\u0669]/g, function (c) {
        return c.charCodeAt(0) - 0x0660;
        }).replace(/[\u06f0-\u06f9]/g, function (c) {
       return c.charCodeAt(0) - 0x06f0;
     });
   } 

    await requestReadSmsPermission();
    await requestReceiveSmsPermission();
  

    const notificationOpen = await firebase
      .notifications()
      .getInitialNotification();

    console.log("notification",notificationOpen);

    if (notificationOpen) {
     console.log(notificationOpen.notification._body);
    
      var str = notificationOpen.notification._body;

      var RefStr = convertNumbers2English(str).match(/واریز\:+(\d+)$/i);
      console.log(RefStr); 
        
      var RefStr1 =convertNumbers2English(str).match(/برداشت\:+(\d+)$/i);
      console.log(RefStr1) 

      var price=RefStr ?RefStr[1] :RefStr1[1]
        console.log(price)
      var headerinput= RefStr ? "واریز" : "برداشت" ;
      this.props.navigation.navigate('Home2', {
        headerinput,
        price,
      });

      await firebase.notifications().removeAllDeliveredNotifications();
    } else {
    }



    this.notificationDisplayedListener = firebase
      .notifications()
      .onNotificationDisplayed((notification) => {
       // console.log(notification)
       console.log(notification._body)
        var str = notification._body;

      var RefStr = convertNumbers2English(str).match(/واریز\:+(\d+)$/i);
      console.log(RefStr); 
        
      var RefStr1 = convertNumbers2English(str).match(/برداشت\:+(\d+)$/i);
      console.log(RefStr1) 

      var price=RefStr ?RefStr[1] :RefStr1[1]
        console.log(price)
      var headerinput= RefStr ? "واریز" : "برداشت" ;
      this.props.navigation.navigate('Home2', {
        headerinput,
        price,
      });
        // this.props.navigation.navigate('Home2');
         firebase.notifications().removeAllDeliveredNotifications();

      });

    // this.notificationOpenedListener = firebase
    //   .notifications()
    //   .onNotificationOpened((notificationOpen) => {
    //             var str = notification._body;

    //   var RefStr = convertNumbers2English(str).match(/واریز\:+(\d+)$/i);
    //   console.log(RefStr); 
        
    //   var RefStr1 = convertNumbers2English(str).match(/برداشت\:+(\d+)$/i);
    //   console.log(RefStr1) 

    //   var price=RefStr ?RefStr[1] :RefStr1[1]
    //     console.log(price)
    //   var headerinput= RefStr ? "واریز" : "برداشت" ;
    //   this.props.navigation.navigate('Home2', {
    //     headerinput,
    //     price,
    //   });
    //     this.props.navigation.navigate('Home2');
    //        // firebase.notifications().removeAllDeliveredNotifications();
    //     const action = notificationOpen.action;
    //     const notification = notificationOpen.notification;
    //   });
  }

  render() {
    return (
      <View style={styles.container}>
        <Text>welcome to home</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    padding: 20,
    flex: 1,
    flexDirection: 'column',
    //  justifyContent:'space-evenly',
    alignItems: 'center',
    backgroundColor: '#416600',
  },
});
