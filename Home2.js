import React, { Component } from 'react';

import {
  TextInput,
  View,
  StyleSheet,
  TouchableOpacity,
  KeyboardAvoidingView,
  Image,
  Text,
  Alert,
} from 'react-native';

import firebase from 'react-native-firebase'
import { StackNavigator } from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import Routes from './Routes';

export default class Home2 extends Component {

  render() {
  const navigation = this.props.navigation;
      console.log(navigation)

    return (

        <View style={styles.container}>

         <Text>welcome to home2</Text>

          <TextInput
            style={styles.input}
            returnKeyType="done"
            placeholder="واریط و برداشت"
            placeholderTextColor="#125485"> {(navigation.getParam("price", 'واریز و برداشتی'))} </TextInput>

          
        </View>
   
    );
  }
}

const styles = StyleSheet.create({

  container: {
    padding: 20,
    flex: 1,
    flexDirection: 'column',
    //  justifyContent:'space-evenly',
    alignItems: 'center',
    backgroundColor: "#002ff5"
  },
  input:{
    fontSize:20,
   
    width:'80%',
    marginTop:20,
    backgroundColor: "#ffffff" 
  }
});
