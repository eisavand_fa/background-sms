import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack'
import Home from './Home';
import Home2 from './Home2';


const Routes = createStackNavigator({
  Home: {
    screen:Home,
    navigationOptions: () => ({
      header:null,
    }),
  },
  Home2: {
    screen:Home2,
    navigationOptions: () => ({
      header:null,
    }),
  },
});

export default createAppContainer(Routes);
