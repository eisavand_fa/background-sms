
import firebase from 'react-native-firebase'

 createNotificationListeners = (title = 'Welcome', body = 'Welcome to Parmi...') => {
  
  try {
    console.log('showLocalPushNotifications');
    const channel = new firebase.notifications.Android.Channel(
       'ChannelID',
        'test',
      firebase.notifications.Android.Importance.Max,
    ).setDescription('test');
    firebase.notifications().android.createChannel(channel);
    const channelGroup = new firebase.notifications.Android.ChannelGroup('test-group', 'Test Channel Group');

    console.log(channel);


    const notification = new firebase.notifications.Notification()
      .setNotificationId(`${Date.now()}`)
      .setSound('default')
      .setTitle(title)
      .setBody(body);
     // .setSmallIcon('ic_launcher'); // create this icon in Android Studio
    // .setColor('#000000');
    notification.android.setChannelId('ChannelID');
    firebase.notifications().displayNotification(notification);
  } catch (e) { 
    console.log(e);
  }
};
