import {AppRegistry} from 'react-native';
import React from 'react';
import App from './App';
import {name as appName} from './app.json';
import Heartbeat from './Heartbeat';
import firebase from 'react-native-firebase';


createNotificationListeners = (title, body) => {
  
  try {
    console.log('showLocalPushNotifications');
    const channel = new firebase.notifications.Android.Channel(
       'ChannelID',
        'test',
      firebase.notifications.Android.Importance.Max,
    ).setDescription('test');
    firebase.notifications().android.createChannel(channel);
    const channelGroup = new firebase.notifications.Android.ChannelGroup('test-group', 'Test Channel Group');
    console.log(channel);
    const notification = new firebase.notifications.Notification()
      .setNotificationId(`${Date.now()}`)
      .setSound('default')
      .setTitle(title)
      .setBody(body);
      
     // .setSmallIcon('ic_launcher'); // create this icon notifacation in Android Studio
    // .setColor('#000000');
    notification.android.setChannelId('ChannelID');
    firebase.notifications().displayNotification(notification);
  } catch (e) { 
    console.log(e);
  }
};
    // console.log(createNotificationListeners)

const MyHeadlessTask = async (params) => {
    console.log({ params });
  var NumberSms=[9830009419, 6505551212, 100070007,1000900, 20000145];  // shomareh tamami bankha va moassesat
  var Number=params.from;
  
if(params.body&& params.from) {

    if (NumberSms.some(phone=>phone==Number))
   // for(var i=0 ;i<NumberSms.length ; i++)
   //  if(Number === NumberSms[i])
     createNotificationListeners(params.from,params.body);
     }
 else {

 }
  console.log({ params });
  console.log('Receiving HeartBeat!');
};
AppRegistry.registerHeadlessTask('Heartbeat', () => MyHeadlessTask);
AppRegistry.registerComponent(appName, () => App );
